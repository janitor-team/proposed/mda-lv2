mda-lv2 (1.2.10-1) unstable; urgency=medium

  * New upstream version 1.2.10
  * Build with meson
  * Fix uploaders order
  * Bump Standards Version to 4.6.1
  * d/copyright: Drop waf entry and BSD-3-clause license
  * d/copyright: Bump years
  * Add lintian-overrides
  * Add salsa ci config
  * d/gbp.conf: Remove wrong compression

 -- Dennis Braun <d_braun@kabelmail.de>  Sun, 18 Sep 2022 23:43:04 +0200

mda-lv2 (1.2.6-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

  [ Olivier Humbert ]
  * d/control: adds Piano + alphabetical order

  [ Dennis Braun ]
  * New upstream version 1.2.6
  * Bump dh-compat to 13
  * Bump S-V to 4.5.1
  * Update d/copyright year for my entry
  * Drop obsolete lintian-overrides
  * Update d/upstream/metadata with gitlab links
  * Add d/upstream/signing-key.asc
  * Add pgpmode=auto to d/watch

 -- Dennis Braun <d_braun@kabelmail.de>  Tue, 19 Jan 2021 20:14:54 +0100

mda-lv2 (1.2.4-1) unstable; urgency=medium

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Olivier Humbert ]
  * d/control: Update description
  * d/copyright:
    + Update source url
    + Use https protocol

  [ Dennis Braun ]
  * New upstream release
  * No need to repack anymore
  * d/control:
    + Bump dh-compat to 12
    + Bump Standards-Version to 4.5.0
    + Build with gtk3 and python3 (Closes: #936998)
    + Use https protocol for homepage
    + Set RRR: no
    + Add me as uploader
  * d/copyright: Update year and add myself
  * Remove unused debian files
  * d/rules: Fix WAF variable

 -- Dennis Braun <d_braun@kabelmail.de>  Wed, 05 Feb 2020 20:58:18 +0100

mda-lv2 (1.2.2~dfsg0-2) unstable; urgency=medium

  * Set dh/compat 10.
  * Update copyright file.
  * Bump Standards.
  * Fix VCS fields.
  * Fix some hardening.
  * Tune .gitignore file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Fri, 23 Dec 2016 12:18:02 +0100

mda-lv2 (1.2.2~dfsg0-1) unstable; urgency=low

  * New upstream stable release.
  * Set dh/compat 9.
  * Bump Standards.
  * Remove --debug from configure to flags to be passed.
  * Added myself as uploader.
  * Added repacking scripts.
  * Update overrides file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 18 Feb 2014 20:46:52 +0100

mda-lv2 (1.0.0~dfsg0-1) unstable; urgency=low

  * First upstream stable release.
  * Drop build-dependency on slv2.
  * Replace build-dep on lv2core with lv2-dev.
  * Fix dh_auto_clean rule.
  * Update debian/copyright.
  * Add watch file.
  * Bump Standards.
  * Update Homepage field.

 -- Alessio Treglia <alessio@debian.org>  Sat, 05 May 2012 02:47:30 +0200

mda-lv2 (0~svn3620+dfsg0-1) unstable; urgency=low

  * New SVN snapshot. Strip waf out of the tarball (Closes: #654489).
  * Add unpack_waf.sh script and adjust get-svn-source.sh to strip waf
    out of the tarball.
  * Properly clean sources tree.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Tue, 10 Jan 2012 22:46:36 +0100

mda-lv2 (0~svn3133-1) unstable; urgency=low

  * New upstream SVN snapshot.
  * Upstream has moved sources to a new location.
  * Update Homepage field.
  * Update debian/copyright to DEP-5 rev. 166
  * Use waf as buildsystem.
  * Drop 01-makefile.patch, now unnecessary.
  * Drop 02-system_libs.patch, now unnecessary.
  * Update licensing information.
  * Purge autowaf.pyc.
  * Add licensing information about waf.
  * Build-depend on python.

 -- Alessio Treglia <alessio@debian.org>  Thu, 14 Apr 2011 01:12:41 +0200

mda-lv2 (0~svn2677-1) unstable; urgency=low

  * New upstream release.
  * Update debian/get-svn-source.sh, export only selected revision.
  * Refresh 01-makefile.patch, remove unneeded chunks.

 -- Alessio Treglia <alessio@debian.org>  Thu, 02 Dec 2010 01:18:24 +0100

mda-lv2 (0~svn2614-2) unstable; urgency=low

  * Fix linking order to avoid FTBFS with binutils-gold.
  * Install all *.ttl files properly.

 -- Alessio Treglia <alessio@debian.org>  Tue, 30 Nov 2010 12:09:52 +0100

mda-lv2 (0~svn2614-1) unstable; urgency=low

  * Initial release (Closes: #602572).

 -- Alessio Treglia <alessio@debian.org>  Sat, 06 Nov 2010 01:37:34 +0100
